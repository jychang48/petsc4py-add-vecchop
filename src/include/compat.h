#ifndef PETSC4PY_COMPAT_H
#define PETSC4PY_COMPAT_H

#include <petsc.h>
#include "compat/mpi.h"
#include "compat/hdf5.h"

#endif/*PETSC4PY_COMPAT_H*/
